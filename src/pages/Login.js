import {useState, useEffect, useContext} from 'react'
import {Container, Form, Button} from 'react-bootstrap'
import UserContext from './../UserContext';
import Swal from "sweetalert2";
import {Redirect} from 'react-router-dom';

export default function Login(){

	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isDisabled, setIsDisabled] = useState(true)

	useEffect( () => {

		if(email !== "" && password !== ""){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}

	}, [email, password])

	function Login(e){
		e.preventDefault()

		
		/*localStorage.setItem(`email`,email)
		setUser({
			email: localStorage.getItem('email')
		})*/

		fetch("https://orig-capstone.herokuapp.com/api/users/login", {
			method: "POST",
			headers:{
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then( response => response.json())
		.then( data => {

			if(data !== "undefined"){
				localStorage.setItem("token", data.access)
				userDetails(data.access)
				Swal.fire({
					title: "Login Successful" ,
					icon: "success",
					text: "Welcome to Course Booking!",

				})
			} else{
				Swal.fire({
					title: "Authentication failed" ,
					icon: "error",
					text: "Check your login details and try again!",
				})
			}
		})

		setEmail("")
		setPassword("")

		const userDetails = (token) => {
			fetch("https://orig-capstone.herokuapp.com/api/users/details",{
				headers:{
					"Authorization": `Bearer ${token}`
				}
			})
			.then( response => response.json())
			.then( data => {
				console.log(data)
				setUser({
					id: data._id,
					isAdmin: data.isAdmin

				})
				
			})	
		}
	}
//console.log(user.id)
	return(

		(user.id !== null) ? //if
			<Redirect to="Courses"/>
		:    //else
			<Container>
				<Form 
					className="border p-3 mb-3"
					onSubmit={ (e) => Login(e) }
					>
				{/*email*/}
				  <Form.Group className="mb-3" controlId="email">
				    <Form.Label>Email address</Form.Label>
				    <Form.Control 
				    	type="email" 
				    	placeholder="Enter email" 
				    	value={email} 
				    	onChange={ (e) => setEmail(e.target.value) }
				    	/>
				  </Form.Group>
				{/*password*/}
				  <Form.Group className="mb-3" controlId="password">
				    <Form.Label>Password</Form.Label>
				    <Form.Control 
				    	type="password" 
				    	placeholder="Password" 
				    	value={password} 
				    	onChange={ (e) => setPassword(e.target.value) }
				    	/>
				  </Form.Group>
				  
				  <Button 
				  		variant="primary" 
				  		type="submit"
				  		disabled={isDisabled}
				  		>
				    Submit
				  </Button>
				</Form>
			</Container>
	)
}
