import {Fragment} from 'react'
import courseData from './../data/courseData';

/*components*/
	/*CourseCard is the template for courses*/
import CourseCard from './../components/CourseCard';

export default function Courses(){

	/*console.log(courseData)*/	//array of objects

	const courses = courseData.map( element => {
		/*console.log(element)*/	//each object in the courseData array

		return(
			<CourseCard key={element.id} courseProp={element}/>
		)
	})

	return (
		<Fragment>
			{courses}
		</Fragment>
	)
}