import Banner from '../components/Banner';

export default function Error(){
	const data = {
		title: "Zuitt Booking",
		content: "Page Not Found",
		link: "/",
		button: "Go back to the homepage"
	}

	return (
		<Banner data={data} />
	)
}
