import {useContext, useEffect} from 'react';
import {Redirect} from 'react-router-dom';
import UserContext from './../UserContext';

export default function Logout(){
	const {unsetUser, setUser} = useContext(UserContext);

	unsetUser();
	/*localStorage.clear()*/

	useEffect(() => {
		setUser({email:null})
	}, [])

	return (
		<Redirect to="Login"/>
	)
}
