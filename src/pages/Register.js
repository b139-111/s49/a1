import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import UserContext from './../UserContext';
import {Redirect} from 'react-router-dom';
import Swal from "sweetalert2";

export default function Register(){

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [cpw, setCpw] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobile, setMobile] = useState('');

  const [register, setIsRegister] = useState(false);
  const [isDisabled, setIsDisabled] = useState(false);
  const {user} = useContext(UserContext);
  
useEffect( () => {
  if(email !== "" && password !== "" && cpw !== "" && firstName !== "" && lastName !== "" && mobile.length > 10 && (password === cpw)){
    setIsDisabled(false)
  } else {
    setIsDisabled(true)
  }

}, [email, password, cpw, firstName, lastName, mobile])

function  Register(e){

  e.preventDefault()
  //fetch for Email Duplicate
  fetch("https://orig-capstone.herokuapp.com/api/users/email-exists", {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json'
      },
      body: JSON.stringify({
          email: email
      })
  })
  .then(res => res.json())
  .then(check => {
      console.log(check)
      if(check !== null){
        fetch("https://orig-capstone.herokuapp.com/api/users/register", {
          method: 'POST',
          headers: {
              'Content-Type': 'application/json'
          },
          body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              password: password
          })
        })
        .then(res => res.json())
        .then(data => {
          console.log(data);
          //If no user information is found, the "access" property will not be available and will return undefined
          if(data){  
              Swal.fire({
                  title: 'Registration Successful!',
                  icon: "success", 
                  text: 'Welcome to Zuitt!'
              })                          
              
              setIsRegister(true);
          }
          else{
            Swal.fire({
                title:'Registration Failed!',
                icon: 'error',
                text: 'Something went wrong. Please try again.'
            })
          }
        })
      }
    }).catch(err => {
          Swal.fire({
              title:`Duplicate email found`,
              icon: 'error',
              text: 'Email already exists!'
          })
    });
  }




    return(

    (user.id !== null) ?
            <Redirect to="Courses"/>
    : (register === true) ?
            <Redirect to="Login"/>    
    :         
    <Container>
      <Form 
        className="border p-3 mb-3"
        onSubmit={ (e) => Register(e)}
        >
        <Form.Group className="mb-3" controlId="firstName">
          <Form.Label>First Name</Form.Label>
          <Form.Control 
            type="text" 
            placeholder="Enter First Name" 
            value={firstName} 
            onChange={ (e) => setFirstName(e.target.value) }
            />
        </Form.Group>
        <Form.Group className="mb-3" controlId="lastName">
          <Form.Label>Last Name</Form.Label>
          <Form.Control 
            type="text" 
            placeholder="Enter Last Name" 
            value={lastName} 
            onChange={ (e) => setLastName(e.target.value) }
            />
        </Form.Group>
        <Form.Group className="mb-3" controlId="email">
          <Form.Label>Email address</Form.Label>
          <Form.Control 
            type="email" 
            placeholder="Enter Email" 
            value={email} 
            onChange={ (e) => setEmail(e.target.value) }
            />
        </Form.Group>
        <Form.Group className="mb-3" controlId="mobile">
          <Form.Label>Mobile Number</Form.Label>
          <Form.Control 
            type="number" 
            placeholder="Enter Mobile Number" 
            value={mobile} 
            onChange={ (e) => setMobile(e.target.value) }
            />
        </Form.Group>
        <Form.Group className="mb-3" controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control 
            type="password" 
            placeholder="Enter Password" 
            value={password}
            onChange={ (e) => setPassword(e.target.value) }
            />
        </Form.Group>
        <Form.Group className="mb-3" controlId="cpw">
          <Form.Label>Confirm Password</Form.Label>
          <Form.Control 
            type="password" 
            placeholder="Verify Password" 
            value={cpw}
            onChange={ (e) => setCpw(e.target.value) }
            />
        </Form.Group>
        <Button 
        variant="primary" 
        type="submit"
        disabled={isDisabled}
        >
          Submit
        </Button>
      </Form>
    </Container>
  )
}
